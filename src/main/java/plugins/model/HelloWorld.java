package plugins.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class HelloWorld {
    private String payload;

    public HelloWorld(String payload) {
        this.payload = payload;

    }
}
