package plugins.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Response {
    private String payload;

    public Response( String payload) {
        this.payload = payload;

    }

}


