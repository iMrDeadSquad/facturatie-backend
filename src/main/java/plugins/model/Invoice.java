package plugins.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Invoice {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "invoice_uuid")
    private String uuid;

    private Integer invoiceNumber;

    private Date invoiceDate;

    private Integer paymentTerm;

    private BigInteger totalPriceExBtw;

    private BigInteger totalPriceIncBtw;

    private String customerUuid;

    @OneToMany(mappedBy = "invoice", fetch = FetchType.LAZY, orphanRemoval = true)
    @Fetch(value = FetchMode.SELECT)
    private List<ProductLine> productLines = new ArrayList<>();

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;



}
