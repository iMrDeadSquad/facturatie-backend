package plugins.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Request {

    private String payload;
    private String pluginID;
    private String pluginName;

    public Request(String payload, String pluginID,String pluginName) {
        this.payload = payload;
        this.pluginID = pluginID;
        this.pluginName = pluginName;

    }

    public String getName() {
        return payload;
    }

    public void setName(String name) {
        this.payload = name;
    }

}
