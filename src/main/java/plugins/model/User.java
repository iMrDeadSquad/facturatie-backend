package plugins.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "user_uuid")
    private String uuid;

    private String companyName;

    private String name;

    private String street;

    private String houseNumber;

    @Size(max = 6)
    private String postalCode;

    private String place;

    private Long phoneNumber;

    private String email;

    private String kvk;

    private String btw;

    private String iban;

}
