package plugins.utils;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class CRMClient {

    private static final String USER_AGENT = "Mozilla/5.0";

    public String RequestToCRM(String jsonWebToken, String URL, String var ) throws IOException {

        // Creates the response and opens the connection.
        String response2 = new String();
        URL obj = new URL(URL + var);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Authorization", jsonWebToken);

        // Gets the response code.
        int responseCode = con.getResponseCode();

        System.out.println("GET Response Code :: " + responseCode);

        // If response code is OK, get the response and returns it.
        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // print result
            System.out.println(response.toString());
            response2 = response.toString();
        } else {        // If something went wrong, it will give this as a response, don't change this!
                        // The plug-in service is build to catch these errors and display the proper warning in the frontend.
            System.out.println("GET request not worked");
            response2 = "GET request not worked, response code " + responseCode;
        }
        return response2;
    }


    // Use this one when you want to use POST request
//
//    private String sendPOST(String jsonWebToken, String URL, String var, String params) throws IOException {
//        String response2 = new String();
//        URL obj = new URL(URL + var);
//        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
//        con.setRequestMethod("POST");
//        con.setRequestProperty("User-Agent", USER_AGENT);
//
//        // For POST only - START
//        con.setDoOutput(true);
//        OutputStream os = con.getOutputStream();
//        os.write(params.getBytes());
//        os.flush();
//        os.close();
//        // For POST only - END
//
//        int responseCode = con.getResponseCode();
//
//        System.out.println("GET Response Code :: " + responseCode);
//
//    // If response code is OK, get the response and returns it.
//        if (responseCode == HttpURLConnection.HTTP_OK) { // success
//        BufferedReader in = new BufferedReader(new InputStreamReader(
//                con.getInputStream()));
//        String inputLine;
//        StringBuffer response = new StringBuffer();
//
//        while ((inputLine = in.readLine()) != null) {
//            response.append(inputLine);
//        }
//        in.close();
//
//        // print result
//        System.out.println(response.toString());
//        response2 = response.toString();
//        } else {        // If something went wrong, it will give this as a response, don't change this!
//        // The plug-in service is build to catch these errors and display the proper warning in the frontend.
//        System.out.println("GET request not worked");
//        response2 = "GET request not worked, response code " + responseCode;
//        }
//        return response2;
//}

}