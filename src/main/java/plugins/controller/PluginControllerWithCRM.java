package plugins.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import plugins.model.HelloWorld;
import plugins.model.Request;
import plugins.model.Response;
import plugins.utils.CRMClient;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class PluginControllerWithCRM {

    @RequestMapping(value = "/makeinvoice")
    public ResponseEntity<?> makeInvoice(@RequestBody Request request, @RequestHeader(HttpHeaders.AUTHORIZATION) String jsonWebToken) throws IOException {
        Response response = new Response();
        // URL of the CRM API for making connection.    //String url = "http://localhost:8080/api/test/admin";
        String url = "http://192.168.189.36:30007/api/test/all";

        try {
            CRMClient crmClient = new CRMClient();
            String var = "";

            try {
                String responseFromCrm  = crmClient.RequestToCRM(jsonWebToken, url, var);

                if (responseFromCrm.contains("GET request not worked, response code")) {
                    response.setPayload("Plug-in krijgt een foutmelding van het CRM, vraag uw admin om advies en geef deze foutmelding door: " + responseFromCrm);
                    return new ResponseEntity<>(response, HttpStatus.CONFLICT);
                }

                else {
                    String payload = "Hello, this is the plugin called Hello World, thanks you for testing this plugin " + request.getPayload() + " with rights testing to access " + responseFromCrm + " using the Auth Microservice";

                    HelloWorld hw = new HelloWorld();
                    hw.setPayload(payload);
                    ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
                    String helloWorldString = ow.writeValueAsString(hw);
                    response.setPayload(helloWorldString);

                    return new ResponseEntity<>(response, HttpStatus.OK);
                }
            }
            catch (Exception e) {
                String payload = "Plug-in kan geen verbinding maken met CRM systeem of krijgt een foutmelding, vraag uw admin om advies";
                response.setPayload(payload);

                return new ResponseEntity<>(response , HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        catch (Exception e) {
            String payload = "Plug-in heeft een foutmelding gehad, vraag uw admin om advies";
            response.setPayload(payload);

        return new ResponseEntity<>(response , HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}

